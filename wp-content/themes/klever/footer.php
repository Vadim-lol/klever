<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package klever
 */

?>

<footer class="footer">
    <div class="container">
        <div class="footer__flex">
            <div class="footer__logo">
                <img src="<?php echo get_template_directory_uri().'/assets/img/logo.svg';?>" alt="" />
            </div>
            <div class="footer__menu">
                <ul>
                    <h3>Меню</h3>
                    <li><a href="про-нас">Про нас</a></li>
                    <li><a href="">Вакансії</a></li>
                </ul>
            </div>
            <div class="footer__menu">
                <ul>
                    <h3>Контакти</h3>
                    <li><a href="tel:+380673757359">+38 (067) 37 57 359</a></li>
                    <li><a href="tel:+380984556361">+38 (098) 45 56 361</a></li>
                </ul>
            </div>
            <div class="footer__menu">
                <ul>
                    <h3>Адреса</h3>
                    <li>м. Хмельницький, вул. Зарічанська, 1</li>
                </ul>
            </div>
            <div class="footer__menu">
                <h3>Соц. мережі</h3>
                <div class="header__social">
                    <a href=""><img src="<?php echo get_template_directory_uri().'/assets/img/viber.svg' ?>" alt="" /></a>
                    <a href=""><img src="<?php echo get_template_directory_uri().'/assets/img/instagram.svg' ?>" alt="" /></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
