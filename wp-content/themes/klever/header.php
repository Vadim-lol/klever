<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package klever
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link
            href="http://fonts.cdnfonts.com/css/gilroy-bold?styles=20876,20878,20879,20880&_v=20220624102434"
            rel="stylesheet"
    />
    <link
            href="http://fonts.cdnfonts.com/css/arkhip?styles=46255&_v=20220624102434"
            rel="stylesheet"
    />

	<?php wp_head(); ?>
</head>

<body>
<header class="header">
    <div class="container">
        <div class="header__flex">
            <div class="header__logo">
                <div class="logo">
                    <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri().'/assets/img/logo.svg';?>" alt="" /></a>
                </div>
            </div>
            <div class="header__menu">
                <ul class="menu">
                    <li>
                        <a href="про-нас">Про нас</a>
                    </li>
                </ul>
            </div>
            <div class="header__contact">
                <a href="tel:+380987235976">+38 (098) 72 35 976</a>
            </div>
            <div class="header__social">
                <a href=""><img src="<?php echo get_template_directory_uri().'/assets/img/viber.svg' ?>" alt="" /></a>
                <a href=""><img src="<?php echo get_template_directory_uri().'/assets/img/instagram.svg' ?>" alt="" /></a>
            </div>
            <div class="header__user">
                <a href="" class="btn"
                ><span>165</span><picture><source srcset="<?php echo get_template_directory_uri().'/assets/img/wish.webp' ?>" type="image/webp"><img src="<?php echo get_template_directory_uri().'/assets/img/wish.png' ?>" alt="" /></picture>
                    <p>Обране</p></a
                >
                <a href="" class="btn btn-white"
                ><img src=" <?php echo get_template_directory_uri().'/assets/img/user.svg' ?>" alt="" />
                    <p>Увійти</p></a
                >
            </div>
        </div>
    </div>
</header>