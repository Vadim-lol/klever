<?php

/*Template name: about*/

get_header();
?>


    <main class="main">
        <section class="about">
            <div class="container">
                <div class="about__flex">
                    <div class="about__item">
                        <div class="about__info">
                            <h3>Чому саме ми?</h3>
                            <p>
                                Наша ціль проста – зробити вибір квартири максимально легким
                                та приємним. Кожен має можливість жити краще! Дуже часто є і
                                доплата на кращу квартиру, і бажання, а як зайдеш на популярні
                                дошки оголошень - відразу хочеться відкласти пошук на не
                                визначений термін!
                            </p>
                            <p>
                                Сьогодні покращення житлових умов в Хмельницькому починається
                                від 85тис.грн. Маючи таку суму доплату вже завтра можна жити в
                                кращій квартирі або в більшій. Це факт, який мало хто розуміє
                                і можливість, якою не користуються.
                            </p>
                        </div>
                        <div class="about__img">
                            <picture>
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/about1.png'; ?>"
                                     alt=""/>
                        </div>
                    </div>
                    <div class="about__item">
                        <div class="about__info">
                            <h3>Принципи роботи</h3>
                            <ul>
                                <li>
                                    Наша ціль проста – зробити вибір квартири максимально легким
                                    та приємним. Кожен має можливість жити краще!
                                </li>
                                <li>
                                    Наша ціль проста – зробити вибір квартири максимально легким
                                    та приємним. Кожен має можливість жити краще!
                                </li>
                                <li>
                                    Наша ціль проста – зробити вибір квартири максимально легким
                                    та приємним. Кожен має можливість жити краще!
                                </li>
                            </ul>
                        </div>
                        <div class="about__img">
                            <picture>
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/about2.png'; ?>"
                                     alt=""/>
                        </div>
                    </div>
                    <div class="about__item">
                        <div class="about__info">
                            <h3>Популярні запитання</h3>
                            <div class="about__info-question">
                                <h4>Питання про 1</h4>
                                <p>
                                    Наша ціль проста – зробити вибір квартири максимально легким
                                    та приємним. Кожен має можливість жити краще! Дуже часто є і
                                    доплата на кращу квартиру.
                                </p>
                            </div>
                            <div class="about__info-question">
                                <h4>Питання про 2</h4>
                                <p>
                                    Наша ціль проста – зробити вибір квартири максимально легким
                                    та приємним. Кожен має можливість жити краще! Дуже часто є і
                                    доплата на кращу квартиру.
                                </p>
                            </div>
                            <div class="about__info-question">
                                <h4>Питання про 3</h4>
                                <p>
                                    Наша ціль проста – зробити вибір квартири максимально легким
                                    та приємним. Кожен має можливість жити краще! Дуже часто є і
                                    доплата на кращу квартиру.
                                </p>
                            </div>
                        </div>
                        <div class="about__img">
                            <picture>
                                <img src="<?php echo get_template_directory_uri() . '/assets/img/about3.png'; ?>"
                                     alt=""/>
                        </div>
                    </div>
                    <div class="about__item">
                        <div class="about__info">
                            <h3>Наші менеджери</h3>
                        </div>
                        <div class="swiper about__slider">
                            <div class="swiper-wrapper">
                                <?php
                                $arg = [
                                    'taxonomy' => 'managers',
                                ];


                                $managers = get_terms($arg);


                                foreach ($managers as $manager) {
                                    ?>
                                    <div class="swiper-slide about__slider-slide">
                                        <div class="about__slide-content">
                                            <div class="about__slider-img">
                                                <img src="<?php $image = get_field('managerPhoto', $manager->taxonomy . '_' . $manager->term_id);
                                                echo $image['url']; ?>" alt=""/>
                                            </div>
                                            <div class="about__slider-info">
                                                <h4><?php echo $manager->name; ?></h4>
                                                <p>
                                                    <?php echo $manager->description; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="swiper-scrollbar about__slider-scrollbar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php

        if (isset($_COOKIE['woocommerce_recently_viewed_2'])) { ?>
            <section class="last" id="">
                <div class="container">
                    <div class="section__title">
                        <h2>Нещодавно переглянуті</h2>
                    </div>
                    <div class="last__post swiper">
                        <div class="swiper-wrapper">
                            <?php
                            $recentlyViewed = explode('|', $_COOKIE['woocommerce_recently_viewed_2']);
                            foreach ($recentlyViewed as $item) {
                                $viewedProduct = wc_get_product($item);
                                ?>
                                <div class="last__post-item swiper-slide">
                                    <a href='<?php echo get_the_permalink($viewedProduct->get_id()) ?>'
                                       class="last__post-img">
                                        <img src="<?php $product_image_url = get_the_post_thumbnail_url($viewedProduct->get_id(), "large");
                                        echo $product_image_url; ?>" alt=""/>
                                    </a>
                                    <div class="last__post-detail">
                                        <h3><?php echo $viewedProduct->get_title(); ?></h3>
                                        <p><?php echo get_field('address'); ?></p>
                                    </div>
                                    <div class="last__post-price">
                                        <p><?php echo $viewedProduct->get_price(); ?> $</p>
                                        <div>
                                            <span><?php echo get_field('area'); ?> м²</span>
                                            <span><?php echo $viewedProduct->get_attribute('number-of-rooms'); ?> кімнати</span>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php }
        ?>
    </main>


<?php

get_footer();
