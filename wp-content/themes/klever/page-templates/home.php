<?php
/*Template name: home*/
get_header();
?>

    <main class="main">
        <section class="hero">
            <div class="container">
                <h1 class="title">Твоє затишне майбутнє</h1>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="home__flex">


                    <?php
                    global $product;
                    $mypost_Query = new WP_Query(array(
                        "post_type" => "product",
                        "post_status" => "publish",
                        "posts_per_page" => 3,
                    ));

                    if ($mypost_Query->have_posts()) {
                        while ($mypost_Query->have_posts()) {
                            $mypost_Query->the_post();
                            ?>
                            <div class="home__item">
                            <a href="<?php echo get_the_permalink($product->get_id() ) ?>" class="home__img">
                                <img src="<?php $post_thumbnail_id = $product->get_image_id();
                                echo wp_get_attachment_url($post_thumbnail_id); ?>" alt=""/>
                            </a>
                            <div class="home__detail">
                            <div class="home__title">
                                <div class="home__title-name">
                                    <h3><?php the_title(); ?></h3>
                                    <p>
                                        <?php echo get_field('address'); ?>
                                    </p>
                                </div>
                                <div class="home__title-whishlist">
                                    <button>
                                        <img src="img/heart.png" alt="">

                                    </button>
                                </div>
                            </div>
                            <div class="home__info">
                            <p>
                                <?php echo $product->get_short_description(); ?>
                            </p>

                            <div class="home__price">
                                <p><?php echo $product->get_price(); ?><span>$</span></p>
                                <span><?php echo get_field('area'); ?> м²</span>
                                <span><?php echo $product->get_attribute('number-of-rooms'); ?> к.кв.</span>
                            </div>
                            <div class="home__manager">
                            <?php $managers = get_the_terms($product->get_id(), 'managers');
                            foreach ($managers as $manager):
                                ?>
                                <div class="manager">
                                    <div class="manager__img">
                                        <img src="<?php $image = get_field('managerPhoto', $manager->taxonomy . '_' . $manager->term_id);
                                        echo $image['url']; ?>"
                                             alt=""/>
                                    </div>
                                    <div class="manager__name">
                                        <h4><?php echo $manager->name; ?></h4>
                                    </div>
                                </div>
                                <div class="manager__viber">

                                    <a href="viber://chat?number=%238<?php the_field('phone1', $manager->taxonomy . '_' . $manager->term_id); ?>">Написати
                                        на вайбер</a>
                                </div>
                                <div class="manager__phone">
                                    <a href="tel:38<?php the_field('phone1', $manager->taxonomy . '_' . $manager->term_id); ?>"><?php
                                        $phone1=get_field('phone1', $manager->taxonomy . '_' . $manager->term_id);
                                        $newPhone1='('.substr($phone1,0,3).') '.substr($phone1,3,3).' '.substr($phone1,6,2).' '.substr($phone1,8,2);
                                        echo $newPhone1;
                                        ?></a>
                                </div>
                                <div class="manager__phone">
                                    <a href="tel:38<?php the_field('phone2', $manager->taxonomy . '_' . $manager->term_id); ?>"><?php
                                        $phone2=get_field('phone2', $manager->taxonomy . '_' . $manager->term_id);
                                        $newPhone2='('.substr($phone2,0,3).') '.substr($phone2,3,3).' '.substr($phone2,6,2).' '.substr($phone2,8,2);
                                        echo $newPhone2;
                                        ?></a>
                                </div>
                                </div>
                                </div>
                                </div>
                                </div>

                            <?php
                            endforeach;
                        }
                    } else {
                        echo("<p>Вибачте, пропозицій поки не існує.</p>");
                    }
                    wp_reset_postdata();
                    ?>


                    <a href="shop" class="btn__home">дивитись більше</a>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="section__title">
                    <h2>Instagram</h2>
                </div>
                <div class="instagram__flex">
                    <a class="instagram__item" href="http://instagram.com">
                        <div class="instagram__img">
                            <picture>
                                <source srcset="img/home1.webp" type="image/webp">
                                <img src="img/home1.png" alt=""/></picture>
                        </div>
                        <div class="instagram__detail">
                            <p>
                                Гарне сучасне планування. Встановлені якісні вхідні броньовані
                                двері, якісні великі металопластикові вікна, сучасний двох
                                контурний котел, на все лічильники, поштукатурені стіни,
                                зроблена мідна електрична розводка. Квартира правильної форми
                                план є на фото.
                            </p>
                        </div>
                    </a>
                    <a class="instagram__item" href="http://instagram.com">
                        <div class="instagram__img">
                            <picture>
                                <source srcset="img/home1.webp" type="image/webp">
                                <img src="img/home1.png" alt=""/></picture>
                        </div>
                        <div class="instagram__detail">
                            <p>
                                Гарне сучасне планування. Встановлені якісні вхідні броньовані
                                двері, якісні великі металопластикові вікна, сучасний двох
                                контурний котел, на все лічильники, поштукатурені стіни,
                                зроблена мідна електрична розводка. Квартира правильної форми
                                план є на фото.
                            </p>
                        </div>
                    </a>
                    <a class="instagram__item" href="http://instagram.com">
                        <div class="instagram__img">
                            <picture>
                                <source srcset="img/home1.webp" type="image/webp">
                                <img src="img/home1.png" alt=""/></picture>
                        </div>
                        <div class="instagram__detail">
                            <p>
                                Гарне сучасне планування. Встановлені якісні вхідні броньовані
                                двері, якісні великі металопластикові вікна, сучасний двох
                                контурний котел, на все лічильники, поштукатурені стіни,
                                зроблена мідна електрична розводка. Квартира правильної форми
                                план є на фото.
                            </p>
                        </div>
                    </a>
                    <a class="instagram__item" href="http://instagram.com">
                        <div class="instagram__img">
                            <picture>
                                <source srcset="img/home1.webp" type="image/webp">
                                <img src="img/home1.png" alt=""/></picture>
                        </div>
                        <div class="instagram__detail">
                            <p>
                                Гарне сучасне планування. Встановлені якісні вхідні броньовані
                                двері, якісні великі металопластикові вікна, сучасний двох
                                контурний котел, на все лічильники, поштукатурені стіни,
                                зроблена мідна електрична розводка. Квартира правильної форми
                                план є на фото.
                            </p>
                        </div>
                    </a>
                    <a class="instagram__item" href="http://instagram.com">
                        <div class="instagram__img">
                            <picture>
                                <source srcset="img/home1.webp" type="image/webp">
                                <img src="img/home1.png" alt=""/></picture>
                        </div>
                        <div class="instagram__detail">
                            <p>
                                Гарне сучасне планування. Встановлені якісні вхідні броньовані
                                двері, якісні великі металопластикові вікна, сучасний двох
                                контурний котел, на все лічильники, поштукатурені стіни,
                                зроблена мідна електрична розводка. Квартира правильної форми
                                план є на фото.
                            </p>
                        </div>
                    </a>
                    <a class="instagram__item" href="http://instagram.com">
                        <div class="instagram__img">
                            <picture>
                                <source srcset="img/home1.webp" type="image/webp">
                                <img src="img/home1.png" alt=""/></picture>
                        </div>
                        <div class="instagram__detail">
                            <p>
                                Гарне сучасне планування. Встановлені якісні вхідні броньовані
                                двері, якісні великі металопластикові вікна, сучасний двох
                                контурний котел, на все лічильники, поштукатурені стіни,
                                зроблена мідна електрична розводка. Квартира правильної форми
                                план є на фото.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </section>
        <section>
            <div class="container">
                <div class="benefits__flex">
                    <div class="benefits__item">
                        <div class="benefits__img">
                            <img src="<?php echo get_template_directory_uri() . '/assets/img/icon1.svg' ?>" alt=""/>
                        </div>
                        <div class="benefits__detail">
                            <h3>1 квартира - 1 оголошення</h3>
                            <p>
                                Всі оголошення унікальні. Одна квартира публікується на сайті
                                тільки 1 раз. Жодних дублів та оголошенm з подібними фото.
                            </p>
                        </div>
                    </div>
                    <div class="benefits__item">
                        <div class="benefits__img">
                            <img src="<?php echo get_template_directory_uri() . '/assets/img/icon2.svg' ?>" alt=""/>
                        </div>
                        <div class="benefits__detail">
                            <h3>Реальні оголошення</h3>
                            <p>
                                Виключно реальні фото і достовірна інформація! Кожне
                                оголошення проходить модерацію “живою людиною”.
                            </p>
                        </div>
                    </div>
                    <div class="benefits__item">
                        <div class="benefits__img">
                            <img src="<?php echo get_template_directory_uri() . '/assets/img/icon3.svg' ?>" alt=""/>
                        </div>
                        <div class="benefits__detail">
                            <h3>Перевірка наявності</h3>
                            <p>
                                Розміщені на сайті квартири кожного місяця проходять повторну
                                перевірку на наявність. У нас немає давно проданих об’єктів.
                            </p>
                        </div>
                    </div>
                    <div class="benefits__item">
                        <div class="benefits__img">
                            <img src="<?php echo get_template_directory_uri() . '/assets/img/icon4.svg' ?>" alt=""/>
                        </div>
                        <div class="benefits__detail">
                            <h3>Зручний</h3>
                            <p>
                                Формуйте власну підбірку цікавих квартир та визначайте
                                найкращі! Соціальні мережі та додаткові контакти допоможуть
                                Вам комфортно та швидко зв’язатися з продавцем.
                            </p>
                        </div>
                    </div>
                    <div class="benefits__item">
                        <div class="benefits__img">
                            <img src="<?php echo get_template_directory_uri() . '/assets/img/icon5.svg' ?>" alt=""/>
                        </div>
                        <div class="benefits__detail">
                            <h3>Місцевий</h3>
                            <p>
                                Сайт зроблений хмельничанами для хмельничан. Ми розуміємо цей
                                ринок і для максимальної зручності підлаштовуємо функціонал
                                під найбільш типові побажання покупців.
                            </p>
                        </div>
                    </div>
                    <div class="benefits__item">
                        <div class="benefits__img">
                            <img src="<?php echo get_template_directory_uri() . '/assets/img/icon6.svg' ?>" alt=""/>
                        </div>
                        <div class="benefits__detail">
                            <h3>Швидкий</h3>
                            <p>
                                Розміщені на сайті квартири кожного місяця проходять повторну
                                перевірку на наявність. У нас немає давно проданих об’єктів.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="question">
            <div class="container">
                <div class="question__flex">
                    <div class="question__title">
                        <h4>Виникли питання?</h4>
                        <p>
                            Безкоштовна консультація з вирішення “квартирних питань”
                            будь-якої складності. Залишай свій номер - ми перетелефонуємо.
                        </p>
                    </div>
                    <form action="">
                        <div class="price__select-item">
                            <span>Ім'я</span>
                            <input type="text" name="" id=""/>
                        </div>
                        <div class="price__select-item">
                            <span>+380</span>
                            <input type="tel" name="" id=""/>
                        </div>
                        <label>
                            <p>Згода на обробку персональних даних</p>
                            <div class="container__cheackbox">
                                <input type="checkbox" name="" id=""/>
                                <span class="checkmark"></span>
                            </div>
                        </label>
                        <button class="btn btn-yelow">відправити</button>
                    </form>
                </div>
            </div>
        </section>
    </main>
<?php
get_footer();