<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( 'last__post-item', $product ); ?>>

    <a href="<?php the_permalink(); ?>">
        <div class="last__post-img">
            <img src="<?php $post_thumbnail_id = $product->get_image_id();
            echo wp_get_attachment_url($post_thumbnail_id); ?>" alt="" />
        </div>
        <div class="last__post-detail">
            <h3><?php the_title(); ?></h3>
            <p><?php echo $product->get_attribute('location');?></p>
        </div>
        <div class="last__post-price">
            <p><?php echo $product->get_price(); ?> $</p>
            <div>
                <span><?php echo $product->get_attribute('ploshcha-zahalʹna'); ?> м²</span>
                <span><?php echo $product->get_attribute('number-of-rooms'); ?> кімнати</span>
            </div>
        </div>
    </a>

</li>
