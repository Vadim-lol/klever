<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

echo $product->get_id();

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
?>

<section class="post">
    <div class="container">
        <div class="post__main">
            <div class="post__slider swiper">
                <div class="swiper-wrapper">
                    <div class="post__slide swiper-slide">
                        <img src="<?php $product_image_url = get_the_post_thumbnail_url($product->get_id(), "large");
                        echo $product_image_url; ?>" alt=""/>
                    </div>

                    <?php // Вывод галереи товаров
                    $attachment_ids = $product->get_gallery_image_ids(); # Получаем id изображений

                    foreach ($attachment_ids as $attachment_id) {
                        if ($attachment_id) { ?>
                            <div class="post__slide swiper-slide">
                                <img src="<?php

                                echo wp_get_attachment_image_src($attachment_id, "full")[0];

                                ?>" alt=""/>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="swiper-button-prev">
                    <img src="<?php echo get_template_directory_uri() . '/assets/img/left.png' ?>" alt="">
                </div>
                <div class="swiper-button-next">
                    <img src="<?php echo get_template_directory_uri() . '/assets/img/right.png' ?>" alt="">
                </div>
            </div>
            <div class="post__price">
                <div class="price__cost">
                    <span><?php echo $product->get_price(); ?> $</span>
                    <p><?php echo get_field('area'); ?> м²</p>
                    <p><?php echo $product->get_attribute('number-of-rooms') ?> к.кв.</p>
                </div>
                <div class="price__manager">
                    <?php $managers = get_the_terms($product->get_id(), 'managers');
                    foreach ($managers as $manager): ?>
                        <div class="manager">
                            <div class="manager__img">
                                <img src="<?php $image = get_field('managerPhoto', $manager->taxonomy . '_' . $manager->term_id);
                                echo $image['url']; ?>" alt=""/>
                            </div>
                            <div class="manager__name">
                                <h4><?php echo $manager->name; ?></h4>
                            </div>
                        </div>
                        <div class="manager__phone">
                            <a href="tel:38<?php the_field('phone1', $manager->taxonomy . '_' . $manager->term_id); ?>"><?php the_field('phone1', $manager->taxonomy . '_' . $manager->term_id); ?></a>
                        </div>
                        <div class="manager__phone">
                            <a href="tel:38<?php the_field('phone2', $manager->taxonomy . '_' . $manager->term_id); ?>"><?php the_field('phone2', $manager->taxonomy . '_' . $manager->term_id); ?></a>
                        </div>
                        <div class="manager__viber">
                            <a href="#">Написати на вайбер</a>
                        </div>
                        <div class="manager__post">
                            <a href="">До оголошень менеджера</a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="post__atribute">
                <div class="atribute atribute-yellow">
                    <p><?php echo $product->get_attribute('type-of-real-estate'); ?></p>
                </div>
                <div class="atribute">
                    <p><?php the_title(); ?></p>
                </div>
                <div class="atribute">
                    <p><?php echo $product->get_attribute('area'); ?></p>
                </div>
            </div>

            <div class="post__title">
                <h1><?php the_title(); ?></h1>
                <p><?php echo get_field('address'); ?></p>
            </div>
            <div class="post__info">
                <h2>Основна інформація</h2>

                <div class="info__flex">
                    <?php
                    echo '<pre>';
                    //print_r(get_product($product->get_id()));
                   // print_r($product->get_attributes());
                    echo '</pre>';
                    //print_r(get_taxonomies());
                    if ($product->get_attributes()) {
                        foreach ($product->get_attributes() as $attribute) { ?>
                            <?php if ($attribute['visible'] > 0) { ?>
                                <div class="info__block">
                                    <h4><?php echo $attribute['name']; ?></h4>
                                    <span><?php

																			foreach ($attribute['options'] as $key => $option) {
																				echo $option . '<br>';
																			}
																			?>
                                        </span>
                                </div>
                            <?php } ?>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="post__description">
                <h2>Опис</h2>
                <div class="post__description-text">
                    <p>
<?php
echo $product->get_short_description();
?>
                    </p>
                </div>
            </div>
            <div class="post__charakter">
                <h2>Споживчі характеристики</h2>
                <div class="post__charakter-text">
                        <?php
                        echo $product->get_description();
                        ?>
                </div>
            </div>
            <div class="post__form">
                <h3>Сподобався об’єкт?</h3>
                <p>
                    Введіть свої контактні дані для заявки і менеджер зв’яжеться з
                    Вами найближчим часом.
                </p>
                <form action="">
                    <div class="price__select-item">
                        <span>Ім'я</span>
                        <input type="text" name="" id=""/>
                    </div>
                    <div class="price__select-item">
                        <span>+380</span>
                        <input type="tel" name="" id=""/>
                    </div>
                    <button type="submit" class="btn btn-yelow">відправити</button>
                </form>
            </div>
        </div>
</section>
<?php

if(isset($_COOKIE['woocommerce_recently_viewed_2'])){ ?>
    <section class="last" id="">
        <div class="container">
            <div class="section__title">
                <h2>Нещодавно переглянуті</h2>
            </div>
            <div class="last__post swiper">
                <div class="swiper-wrapper">
                    <?php
                    $recentlyViewed=explode('|',$_COOKIE['woocommerce_recently_viewed_2']);
                    foreach ($recentlyViewed as $item){
                        $viewedProduct=wc_get_product($item);
                        ?>

                        <div class="last__post-item swiper-slide">
                            <a href='<?php echo get_the_permalink($viewedProduct->get_id() ) ?>'class="last__post-img">
                                <img src="<?php $product_image_url = get_the_post_thumbnail_url($viewedProduct->get_id(), "large");
                                echo $product_image_url; ?>" alt=""/>
                            </a>
                            <div class="last__post-detail">
                                <h3><?php echo $viewedProduct->get_title(); ?></h3>
                                <p><?php echo $viewedProduct->get_attribute('location');?></p>
                            </div>
                            <div class="last__post-price">
                                <p><?php echo $viewedProduct->get_price(); ?> $</p>
                                <div>
                                    <span><?php echo $viewedProduct->get_attribute('ploshcha-zahalʹna'); ?> м²</span>
                                    <span><?php echo $viewedProduct->get_attribute('number-of-rooms'); ?> кімнати</span>
                                </div>
                            </div>
                        </div>


                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php }
?>

</div>

<?php

do_action('woocommerce_after_single_product'); ?>
