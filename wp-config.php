<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'klever2' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost:3307' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ltM}:s/}W_<{zsDjL|oTm 9[sHMc!hqKiKrgNr|r{a$6e-#,N8MhIfy9 ajCBjJ8' );
define( 'SECURE_AUTH_KEY',  '@EcHzv<*P(Sq5r&0cm?/2:tcV|RwRY,,~srtwM> ehQ@@tnVHL([>K6-9oF-@X(W' );
define( 'LOGGED_IN_KEY',    'Y[dUb3)R2d%03tGQPfm?#OKcFAsS#aV<x5QMb>1Q:5^v%2{{?{]?9^H`N5b7T?aE' );
define( 'NONCE_KEY',        '}J17+l4gx/,;MjBhR`VQMJCiqix`gZi6N.It`qZj% M.g#xWohi.1{QcPxmvK]R*' );
define( 'AUTH_SALT',        '9OO2S(n3Z!?%<{|C(u2:|^[pzx6(H+d 8&l4mmidHY^CDo19db{,$V}lt~1>f[b3' );
define( 'SECURE_AUTH_SALT', 't@ZW0W{_H<$EMZ,hMgIip3$YIHDC69<`%fk18`-Qgk}v.QVcW<$tE$Z` ~FSArV`' );
define( 'LOGGED_IN_SALT',   '?p&z?)-0LwX%@}b%YA:&PSAU}&1:$Iu/yf)3C$?XnrRlEg;j8Mcaxws2]`}q]RP{' );
define( 'NONCE_SALT',       '5$1qwImMssOs{%2OX.H?.$pyBetL7lzRBJ.0F]B}p6/>pZ)>N70Z{$n{_7E_z`JI' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
